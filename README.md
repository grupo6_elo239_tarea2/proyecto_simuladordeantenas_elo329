# Proyecto Simulador de Antenas - ELO329

## Grupo 5:
- Sergio Ehlen Montero / 202130016-3
- Camilo Troncoso Hormazabal / 202130004-k
- Maximo Flores Suarez / 202130019-8
- Cristian González Bustos / 201704003-3

## Requisitos:
- JDK versión mínimo 15
- JavaFx versión mínimo 20.0.1

## Compilar y correr:

1. Reemplazar en el archivo Makefile la variable JFX_OPTIONS con la dirección de la librería de JavaFx.
2. Abrir en una terminar el directorio /src
3. Ejecutar: < make >
4. Una vez terminado, ejecutar con < make run >

## Para usar:

1. Ingresar la configuración de la simulación en la ventana de config.
2. Apretar click derecho en la ventana principal para agregar una antena.
3. Configurar las especificaciones de la antena.
4. Repetir los pasos 2 y 3 para crear la red de antenas que se desea simular.


#### Para más información, revisar el archivo HTML del informe