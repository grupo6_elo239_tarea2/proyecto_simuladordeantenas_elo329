import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.*;
import javafx.scene.media.Media;
import java.util.ArrayList;

///////   Sim.java    ///////

/**
 * Reepresenta el pane principal donde ocurre la simulación. Extiende BorderPane de JavaFx. Dado que el programa solo corre una a la vez,
 * la mayoría de sus métodos y atributos son estáticos, pero para utilizar los heredados de BordeerPane igual necesitamos
 * una instancia de Sim.
 */


public class Sim extends BorderPane
{
    /**
     * Constructor de Sim. Genera las top bar, menues de contexto, tamaños y defaults
     */
    public Sim()
    {
        antenas = new ArrayList<>();
        setDefaults();
        makeBg();
        makeBar();
        makeContext();
        setPrefHeight(600);
        setPrefWidth(1000);
    }

    /**
     * Esta función se llama cada vez que ocurre un cambio que puede afectar la potencia transferida.
     * Permite re calcular la potencia recibida en cada antena receptora, acumulando lo que le otorga cada
     * antena transmisora.
     */
    public static void updateTransference(){
        for (Antena Rx : antenas)
        {if (!Rx.isTransmiting()){
            Rx.reset();
            Rx.getView().clearLinks();
            for (Antena Tx:antenas){if (Tx.isTransmiting){
                Tx.getView().txupdate();
                Rx.link(Tx);
            }
            }
        }
        }
    }

    /**
     * Permite obtener la longitud de onda a trabajar en la simulación.
     * @return lambda       Longitud de ondas en metros
     */
    public static double getLambda() {return c / frec;}

    /**
     * Retorna la frecuencia de operación de las antenas.
     * @return frecuencia   Frecuencia en hz
     */
    public static double getFrec() {return frec;}

    /**
     * Retorna un objeto de tipo scale con la escala de amplificación usada.
     * @return scale        Escala siendo usada
     */
    public static Scale getScale() {return scale;}

    /**
     * Retorna un objeto de tipo Media con el audio que se va a usar en los receptores.
     * @return media        Audio configurado
     */
    public static Media getSound() {return sound;}

    /**
     * Retorna una lista con todas las antenas actualmente participando en la simulación.
     * @return antenas      Arraylist con todas las antenas en uso
     */
    public static ArrayList<Antena> getAntenas() {return antenas;}

    private void setDefaults(){
        frec = 1000000; //1[Mhz] es una buena base como default
        scale = new Scale(1000); //Mejor dejar el default en 1km para evitar muchos problemas con campo lejano
        weathAttenuation = 1;
        sound = new Media(Config_Stage.class.getResource("resources/default.wav").toExternalForm());
        useDBM = false;
    }

    private void makeBg(){
        BackgroundImage bg = new BackgroundImage(new Image
                ("resources/grid_custom.jpg",400,400,false,true),
                BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT);
        setBackground(new Background(bg));
    }
    private void makeBar(){
        MenuBar barra = new MenuBar();

        Menu file = new Menu("File");

        Menu config = new Menu("Config");
        final MenuItem preferences = new MenuItem("Preferences");
        preferences.setAccelerator(KeyCombination.keyCombination("CTRL+S"));
        preferences.setOnAction(e->{
            Config_Stage c = new Config_Stage();});
        config.getItems().add(preferences);

        Menu help = new Menu("Help");

        barra.getMenus().addAll(file,config,help);

        setTop(barra);
    }
    private void makeContext(){
        ContextMenu contextMenu = new ContextMenu();
        MenuItem newAntena = new MenuItem("New antena");
        //newAntena.setAccelerator(KeyCombination.keyCombination("CTRL+A"));
        contextMenu.getItems().add(newAntena);
        this.setOnMouseClicked(event->{
            if (event.getButton() == MouseButton.SECONDARY){
                mouseX = event.getX();
                mouseY = event.getY();
                contextMenu.show(this,event.getScreenX(),event.getScreenY());
            }
            else contextMenu.hide();
        });
        newAntena.setOnAction(event->{
            Antena_Stage crear = new Antena_Stage(this,mouseX,mouseY);
        });
    }
    static boolean useDBM;
    static double frec;
    static Scale scale;
    static double weathAttenuation;
    static Media sound;
    static double c = 3*Math.pow(10,8); //velocidad de la luz
    private double mouseX;
    private double mouseY;
    private static ArrayList<Antena> antenas;
}
