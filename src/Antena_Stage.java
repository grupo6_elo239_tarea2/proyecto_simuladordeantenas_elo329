import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

///////   Antena_Stage.java    ///////

/**
 * Representa la ventana que se abre para crear o editar una antena. Extiende la clase Stage de javaFx y su único
 * uso en el código es llamar a su constructor. El resto de los metodos son privados e internos.
 */

public class Antena_Stage extends Stage
{
    /**
     * Constructor de nueva antena. Pide solamente una coordenada X,Y en la cual se pondrá una nueva antena al
     * el botón de confirmación.
     * @param home      Instancia del Sim en la cual se mostrará la antena
     * @param x         Coordenada X donde se pondrá la antena
     * @param y         Coordenada Y donde se pondrá la antena
     */
    public Antena_Stage(Sim home, double x, double y)
    {
        this.home = home;
        window = new VBox();
        makeButtons();
        updateLowerHalf();
        Scene scene = new Scene(window);
        setTitle("Nueva antena");
        this.x = x;
        this.y = y;
        setScene(scene);show();
    }

    /**
     * Constructor de editar antena. Pide por argumento la antena, la cual será reemplazada por una antena con
     * nuevas especificaciónes al apretar el botón de confirmación.
     * @param home      Instancia del Sim en la cual se mostrará la antena
     * @param antena    Antena a editar
     */
    public Antena_Stage(Sim home, Antena antena)
    {
        this.home = home;
        this.antena = antena;
        window = new VBox();

        makeButtons();
        updateLowerHalf();
        loadsettings();
        Scene scene = new Scene(window);
        setTitle(antena.getName());
        setScene(scene);show();

    }
    private void makeButtons(){
        setAlwaysOnTop(true);
        this.setWidth(400);
        this.setHeight(250);
        HBox buttons = new HBox();
        Button finButton = new Button("Ok!");
        Button closeButton = new Button("Cancel");
        buttons.getChildren().addAll(finButton,closeButton);
        closeButton.setOnAction(e->this.close());
        finButton.setOnAction(e->createAntena());
        addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode()== KeyCode.ENTER){finButton.fire();}
            else if (event.getCode()==KeyCode.ESCAPE){closeButton.fire();}
        });

        window.getChildren().addAll(makeUpper(),buttons);
    }
    private Pane makeUpper(){
        HBox trCheckLine = new HBox();
        trCheck = new CheckBox();
        trCheck.setSelected(true);
        trCheckLine.getChildren().addAll(new Label("Transmitiendo: "),trCheck);

        HBox powLine = new HBox();
        powIn = new TextField();
        powLine.getChildren().addAll(new Label("Potencia: "),powIn,new Label("[W]"));

        trCheck.setOnAction(e->{
            if (trCheck.isSelected())powIn.setDisable(false);
            else powIn.setDisable(true);
        });

        HBox selectLine = new HBox();
        selectIn = new ComboBox(FXCollections.observableArrayList(
                new String[]{"------","Isotropica", "Dipolo","Parabolica"}));
        selectIn.setValue("------");
        selectLine.getChildren().addAll(new Label("Tipo de antena: "),selectIn);
        main = new VBox();
        selectIn.setOnAction(event ->updateLowerHalf());

        effIn = new Spinner<Double>();
        effIn.setValueFactory(
                new SpinnerValueFactory.DoubleSpinnerValueFactory(0,1,1,0.01));
        effIn.setEditable(true);
        main.getChildren().addAll(trCheckLine,powLine,selectLine,makeLowIso());
        return main;
    }
    private void updateLowerHalf(){
        //Esta primera parte es sensible, y puede causar problemas si el menu config se agranda hacia abajo
        main.getChildren().remove(main.getChildren().size()-1);
        switch((String) selectIn.getValue()){
            case "Isotropica"->
                    main.getChildren().add(makeLowIso());
            case "Dipolo"->
                    main.getChildren().add(makeLowDipole());
            case "Parabolica"->
                    main.getChildren().add(makeLowPara());
            default->
                    main.getChildren().add(new Label("NULL"));
        }
    }
    private Pane makeLowIso(){
        //Este es un poco redundante para la poca configuración que tiene una isotropica
        //Pero la idea es que sea relativamente estandarasado para todas las antenas
        HBox h = new HBox();
        IsoAntenaView onlyView = new IsoAntenaView(home,false);
        VBox antenabox = new VBox(onlyView);
        //antenabox.setPadding(new Insets(20,20,20,20));
        VBox conf = new VBox();
        HBox effLine = new HBox();
        effLine.getChildren().addAll(new Label("Eficiencia: "),effIn);
        effLine.setAlignment(Pos.TOP_CENTER);
        conf.getChildren().addAll(effLine);
        h.getChildren().addAll(antenabox,conf);
        return h;
    }
    private Pane makeLowDipole(){
        HBox h = new HBox();
        DipoleAntenaView onlyView = new DipoleAntenaView(home,false);
        VBox antenabox = new VBox(onlyView);
        //antenabox.setPadding(new Insets(20,20,20,20));
        VBox conf = new VBox();
        HBox effLine = new HBox();
        effLine.getChildren().addAll(new Label("Eficiencia: "),effIn);
        typeIn = new ComboBox(FXCollections.observableArrayList(
                new String[]{"Full Lambda","Half Lambda"}));
        typeIn.setValue("Full Lambda");
        HBox typeLine = new HBox();
        typeLine.getChildren().addAll(new Label("Tipo de dipolo: "),typeIn);
        //effLine.setAlignment(Pos.TOP_CENTER);
        //typeLine.setAlignment(Pos.TOP_CENTER);
        conf.getChildren().addAll(effLine,typeLine);
        h.getChildren().addAll(onlyView,conf);
        return h;
    }

    private Pane makeLowPara(){
        HBox h = new HBox();
        ParabolicAntenaView onlyView = new ParabolicAntenaView(home, false);
        VBox antenabox = new VBox(onlyView);
        //antenabox.setPadding(new Insets(20,20,20,20));
        VBox conf = new VBox();
        HBox effLine = new HBox();
        effLine.getChildren().addAll(new Label("Eficiencia "),effIn);
        HBox diamLine =  new HBox();
        diamIn = new TextField();
        diamLine.getChildren().addAll(new Label("Diametro: "),diamIn, new Label("[m]"));
        conf.getChildren().addAll(effLine,diamLine);
        h.getChildren().addAll(onlyView,conf);
        return h;
    }

    private void loadsettings(){
        trCheck.setSelected(antena.isTransmiting());
        if (antena.isTransmiting()) powIn.setText(String.valueOf(antena.getPotencia()));
        else powIn.setDisable(true);

        if (antena instanceof IsoAntena){
            selectIn.setValue("Isotropica");
            updateLowerHalf();
        } else if (antena instanceof DipoleLambdaAntena) {
            selectIn.setValue("Dipolo");
            updateLowerHalf();
            typeIn.setValue("Full Lambda");
        } else if ( antena instanceof DipoleHalfLambdaAntena){
            selectIn.setValue("Dipolo");
            updateLowerHalf();
            typeIn.setValue("Half Lambda");
        }else if ( antena instanceof ParabolicAntena){
            selectIn.setValue("Parabolica");
            updateLowerHalf();
            diamIn.setText(String.valueOf(((ParabolicAntena) antena).getDiameter()));
        }else selectIn.setValue("------");
        //updateLowerHalf();
        effIn.getValueFactory().setValue(antena.getEfficiency());
    }

    private void createAntena(){
        //Esta función está un poco sobrecargada y necesita ser redistribuida eventualmente
        AntenaView v;
        if (antena!=null){
            Sim.getAntenas().remove(antena);
            antena.view.clearLinks();
            home.getChildren().remove(antena.view);
            home.getChildren().remove(antena.getView().getLabelBox());
            x = antena.getX();
            y = antena.getY();
            antena.returnName();
        }
        switch ((String) selectIn.getValue())
        {
            case "Isotropica" ->
            {
                v = new IsoAntenaView(home,true);
                if (trCheck.isSelected()){
                    antena = new IsoAntena(effIn.getValue(),
                            Double.parseDouble(powIn.getText()),(IsoAntenaView) v);
                }
                else {
                    antena = new IsoAntena(effIn.getValue(),
                            (IsoAntenaView) v);
                }
            }
            case "Dipolo"->
            {
                v = new DipoleAntenaView(home,true);
                if (typeIn.getValue()=="Half Lambda"){
                    if (trCheck.isSelected())
                        antena = new DipoleHalfLambdaAntena(effIn.getValue()
                                ,Double.parseDouble(powIn.getText()),(DipoleAntenaView) v);
                    else antena = new DipoleHalfLambdaAntena(effIn.getValue()
                            ,(DipoleAntenaView) v);
                }
                else { //Dipolo Lambda completo
                    if (trCheck.isSelected())
                        antena = new DipoleLambdaAntena(effIn.getValue()
                                ,Double.parseDouble(powIn.getText()),(DipoleAntenaView) v);
                    else antena = new DipoleLambdaAntena(effIn.getValue()
                            ,(DipoleAntenaView) v);
                }
            }
            case "Parabolica"->
            {
                v = new ParabolicAntenaView(home, true);
                if (trCheck.isSelected()){
                    antena = new ParabolicAntena(effIn.getValue(),
                            Double.parseDouble(powIn.getText()),Double.parseDouble(diamIn.getText()),(ParabolicAntenaView) v);
                }
                else {
                    antena = new ParabolicAntena(effIn.getValue(),
                            Double.parseDouble(diamIn.getText()),(ParabolicAntenaView) v);
                }
            }
            default ->
            {
                System.out.println("Deleted antena");
                Sim.updateTransference();
                close();
                return;
            }
        }
        v.setModel(antena);
        v.setHome(home);
        home.getChildren().add(v);
        antena.move(x,y);
        Sim.getAntenas().add(antena);
        Sim.updateTransference();
        close();
    }


    private double x ; private double y;
    private Antena antena;
    private Sim home;
    private CheckBox trCheck;
    private TextField powIn;
    private Spinner<Double> effIn;
    private TextField diamIn;
    private VBox main;
    private VBox window;
    private ComboBox selectIn;
    private ComboBox typeIn;
}
