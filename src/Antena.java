///////   Antena.java    ///////

/**
 * Clase abstracta de la que heredan todos los otros modelos de antena. Define funciones genéricas como mover,
 * rotar, y calcular potencia transferida, al igual que atributos genéricos como coordenadas, ángulo, eficiencia,
 * potencia, id y vista.
 */

public abstract class Antena
{
    /**
     * Ganancia directiva de la antena con respecto a otra antena con una inclinación de alpha. Este
     * no es necesariamente el ángulo de radiación, pues internamente se hace la suma con la inclinación de
     * la antena misma.
     * @param alpha         Ángulo entre las posiciónes de las antenas
     * @return gain         Ganancia directiva
     */
    public abstract double directionGain(Angle alpha);

    /**
     * Retorna el nombre de la antena con el formato "Tipo de antena + id"
     * @return name     String con el nombre de la antena con el formato "Tipo de antena + id"
     */
    public abstract String getName();

    /**
     * La antena obtiene un id del stack y lo vuelve no-disponble a otras antenas de el mismo tipo.
     */
    abstract public void drawName();

    /**
     * La antena retorna su id al stack y lo vuelve disponible a otras antenas del mismo tipo.
     */
    abstract public void returnName();

    /**
     * Traslada a la antena a una nueva coordenada. Si esta tiene una vista, esta se mueve tambien. Se define X positivo
     * de izquierda a derecha e Y positivo como arriba hacia abajo. Los valores de x e y se miden en pixeles.
     * @param x         Nueva coordenada X
     * @param y         Nueva coordenada Y
     */
    public void move(double x, double y){
        this.x = x;
        this.y = y;
        if (view!=null){
            view.setCenterX(x);
            view.setCenterY(y);
            view.clearLinks();
            view.relocate(x-view.getWidth()/2,y-view.getHeight()/2);
            view.getLabelBox().relocate(50+x-view.getWidth()/2,50+y-view.getHeight()/2);
            Sim.updateTransference();
        }
    }

    /**
     * Rota la antena a una nueva alineación. Si tiene una vista, esta rota tambien. Se define el angulo como naciente
     * en el horizonte derecho y positivo en sentido horario.
     * @param deg       Nuevo ángulo al cual apunta la antena
     */
    public void rotate(Angle deg){
        this.angle.setVal(deg.getDeg(),true);
        view.setRotate(angle.getDeg());
        Sim.updateTransference();
    }

    /**
     * Reinicia el cálculo de la potencia de la antena. Sólo funciona en receptores.
     * Es importante hacer esto cada vez que se re-calcule la potencia recibida.
     */
    public void reset(){
        if (!isTransmiting){
            potencia = 0;
            reference = 0;
        }
    }

    /**
     * Establece un enlace de trasnferencia entre esta antena y la antena Tx. Como convenio, esta funciona se
     * llama solamente en el receptor. Automáticamente, construye el enlace visual que conecta la vista de las
     * antenas.
     * @param Tx        Antena transmitiendo potencia
     */
    public void link(Antena Tx){
        System.out.println("linking: "+this.getName()+" to "+Tx.getName());

        double rPow = 1;
        double dX = getView().getCenterX()- Tx.getView().getCenterX();
        double dY = getView().getCenterY()- Tx.getView().getCenterY();
        double distance = Math.sqrt(Math.pow(dX,2)+ Math.pow(dY,2))*Sim.getScale().getValScaled();
        Angle alpha = new Angle(dX,dY);

        System.out.println("Tx angle: "+Tx.getAngle().getDeg()+" , Rx angle: "+getAngle().getDeg());
        System.out.println("Alpha: "+alpha.getDeg());

        rPow *= this.directionGain(alpha);
        rPow *= Tx.directionGain(alpha);
        rPow /= Math.pow((4*Math.PI*distance)/(Sim.c/Sim.getFrec()),2);

        reference += Tx.getPotencia();
        potencia += rPow;

        if (potencia==Double.POSITIVE_INFINITY
                || potencia==Double.NEGATIVE_INFINITY
                || potencia > reference) {
            potencia = 0;
            view.link(Tx.getView(),true);
        }
        else
            view.link(Tx.getView(),false);

    }

    /**
     * Getter para saber si la antena es un transmisor o receptor
     * @return isTransmiting        booleano true si la antena es transmisor, false si es receptor
     */
    public boolean isTransmiting() {return isTransmiting;}

    /**
     * Getter para saber cuanta potencia la antena está transmitiendo. Si la antena es receptor, retorna cuanta
     * potencia está recibiendo.
     * @return  potencia            Potencia en Watts
     */
    public double getPotencia() {return potencia;}

    /**
     * Getter para saber cuanta potencia están transmitiendo las antenas transmisores en la simulación. Se usa
     * para escalar el volumen apropiadamente.
     * @return  reference           Potencia total siendo entregada por transmisores en Watts
     */
    public double getReference() {return reference;}

    /**
     * Getter para obtener el ángulo de inclinación de la antena
     * @return  angle               Ángulo de inclinación de la antena
     */
    public Angle getAngle() {return angle;}

    /**
     * Eficiencia de la antena. Se define entre los valores de 0 a 1
     * @return  efficiency          Eficiencia de la antena
     */
    public double getEfficiency() {return efficiency;}

    /**
     * Retorna la vista asociada a esta antena.
     * @return  view                Vista asociada
     */
    public AntenaView getView() {return view;}

    /**
     * Retorna la posición de la antena en la coordenada X
     * @return x        coordenada x del centro de la antena
     */
    public double getX() {return x;}
    /**
     * Retorna la posición de la antena en la coordenada X
     * @return          coordenada y del centro de la antena
     */
    public double getY() {return y;}

    /**
     * Potencia de referencia siendo transmitida en total por los otros transmisores sin contar atenuación en Watt.
     */
    protected double reference;
    /**
     * Booleano. True si la antena es transmisor, False si es receptor.
     */
    protected boolean isTransmiting;
    /**
     * Potencia de la antena en Watt. Si es un transmisor, representa la potencia que está transmitiendo.
     * Si receptor, representa la potencia que está recibiendo.
     */
    protected double potencia;
    /**
     * Valor de la posición en el eje X de la antena. Se deine positivo de izquierda a derecha.
     */
    protected double x;
    /**
     * Valor de la posición en el eje Y de la antena. Se define positivo de arriba hacia abajo.
     */
    protected double y;
    /**
     * Ángulo de inclinación de la antena con respecto al eje X positivo. Se define
     * positivo en dirección horaria.
     */
    protected Angle angle;
    /**
     * Eficiencia de la antena. Toma valores en el rango [0,1}.
     */
    protected double efficiency;
    /**
     * Vista de la antena. Debido a que esta tambien es abstracta, siempre será
     * una clase hijo la vista al igual que la antena.
     */
    protected AntenaView view;
    /**
     * Id de la antena. Se utiliza para identificar antenas del mismo tipo.
     */
    protected int id;
}
