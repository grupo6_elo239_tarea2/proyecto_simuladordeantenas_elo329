import java.util.Arrays;
import java.util.PriorityQueue;

///////   DipoleHalfLambdaAntena.java    ///////

/**
 * Modelo de una antena de tipo prabólica. Extiende la clase Antena, y posee metodos propios
 * para crear sus propios nombres. Puede ser creada como receptor o transmisor.
 */
public class ParabolicAntena extends Antena
{
    /**
     * Constructor de una antena prabólica transmisora.
     * @param efficiency        Valor de eficiencia de la antena en rango [1,0]
     * @param potencia          Potencia siendo transmitida en Watts
     * @param diameter          Diametro de la antena parabólica en metros
     * @param view              Vista de la antena
     */
    public ParabolicAntena(double efficiency, double potencia,double diameter,ParabolicAntenaView view)
    {
        angle = new Angle(0,false);
        isTransmiting = true;
        this.diameter = diameter;
        this.potencia = potencia;
        this.efficiency = efficiency;
        this.view = view;
        drawName();
    }

    /**
     * Constructor de una antena parabólica receptora.
     * @param efficiency        Valor de la eficiencia de la antena en rango [0,1]
     * @param diameter          Diametro de la antena parabólica en metros
     * @param view              Vista de la antena
     */
    public ParabolicAntena(double efficiency,double diameter ,ParabolicAntenaView view)
    {
        angle = new Angle(0,false);
        isTransmiting = false;
        this.diameter = diameter;
        this.potencia = 0;
        this.efficiency = efficiency;
        this.view = view;
        drawName();
    }

    /**
     * Ganancia directiva de la antena con respecto a otra antena con una inclinación de alpha. Este
     * no es necesariamente el ángulo de radiación, pues internamente se hace la suma con la inclinación de
     * la antena misma.
     * @param alpha         Ángulo entre las posiciónes de las antenas
     * @return gain         Ganancia directiva
     */
    @Override
    public double directionGain(Angle alpha)
    {
        Angle theta;
        if (isTransmiting) theta = new Angle(alpha.minus(angle).getDeg(),true);
        else theta = new Angle(alpha.minus(angle).minus(Angle.Pi()).getDeg(),true);

        Double c = Math.PI * diameter / Sim.getLambda();
        System.out.println(c);
        Angle limit = new Angle(Math.asin(3.83/c),false);

        System.out.println("Theta: "+theta.getDeg());
        System.out.println("Limit: "+limit.getDeg());
        System.out.println("Limit comp: "+limit.restofRotation().getDeg());
        if (3.83/c >= Math.PI/2){
            System.out.println("!!Warning: "+getName()+" is operating with a radiation angle of less than 0");
        }


        if (theta.lessThan(limit) || theta.moreThan(limit.restofRotation())){
            return efficiency * Math.pow(c,2);
        }
        else return 0;
    }

    /**
     * Retorna el diametro de la antena parabólica.
     * @return diameter     Diametro de la antena en metros
     */
    public double getDiameter(){return diameter;}

    /**
     * La antena obtiene un id del stack y lo vuelve no-disponble a otras antenas de el mismo tipo.
     */
    public void drawName(){
        id = nameList.remove();
    }

    /**
     * La antena retorna su id al stack y lo vuelve disponible a otras antenas del mismo tipo.
     */
    public void returnName(){
        nameList.add(id);
    }

    /**
     * Retorna el nombre de la antena con el formato "Tipo de antena + id"
     * @return name     String con el nombre de la antena con el formato "Tipo de antena + id"
     */
    public String getName() {
        return "Parabolica "+id;
    }
    private static PriorityQueue<Integer> nameList;
    static {
        nameList = new PriorityQueue<>(Arrays.asList(10,9,8,7,6,5,4,3,2,1));
    }
    private double diameter;
}
