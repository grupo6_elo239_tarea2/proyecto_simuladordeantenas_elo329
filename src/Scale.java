///////   Scale.java    ///////

/**
 * Tipo de dato no primitivo que describe la escala de crecimiento a aplicar
 * a las distancias en el simulador. Se implementó para facilitar la configuración
 * de este parametro en el Config_Stage
 */


public class Scale
{
    /**
     * Constructor de escala a base del factor de amplificación
     * @param val       Factor de amplificación
     */
    public Scale(long val)
    {
        setVal(val);
    }

    /**
     * Constructor de escala a base del string que lo describe
     * @param string     String de escala
     */
    public Scale(String string)
    {
        setString(string);
    }

    /**
     * Retorna la forma string del factor de amplificación
     * @return string       factor de amplificación
     */
    public String getString()
    {return string;}

    /**
     * Retorna el factor de amplificación de la escala
     * @return  val         factor de amplificación
     */
    public long getVal()
    {return val;}

    /**
     * Retorna el factor de amplificación escalado a 1000 pixeles
     * @return  val         factor de amplificación escalado
     */
    public double getValScaled()
    {return val/1000;}


    /**
     * Define el valor de el factor de aplificación a base de un string. El string
     * tiene que ser uno especifico, perteneciente a la lista de opciones.
     * @param string        factor de amplificación nuevo
     */
    public void setString(String string)
    {
        for (int i = 0; i < opts.length; i++)
        {
            if (string == opts[i]){
                this.string = string;
                val = (long) Math.pow(10,i+1);
                return;
            }
        }
    }

    /**
     * Define la magnitud del factor de amplificación a base de un long. El long tiene
     * que ser uno especifico, y equivalente a alguna de las opciones.
     * @param val   valor a fijar
     */
    public void setVal(long val)
    {
        if (val%10!=0){return;}
        string = opts[(int) (Math.log10(val)-1)];
    }

    /**
     * Funciona estatica que retorna una lista con las opciones que se pueden tomar para
     * las escalas. Se usa en el comboBox de Config_Stage.
     * @return  opts        arreglo de strings con los posibles valores que puede tomar el scale
     */
    public static String[] getopts(){return opts;}
    private static String opts[]= {"10[m]","100[m]","1[km]","10[km]","100[km]","1000[km]"};
    private String string;
    private long val;
}
