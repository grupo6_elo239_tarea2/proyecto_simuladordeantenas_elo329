import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import java.util.ArrayList;

///////   AntenaView.java    ///////

/**
 * Clase abstracta de la que heredan todas las otras vistas de antena. Define funcionamientos genéricos, como
 * el menu de contexto y manejo de lables y linklines.
 */

public abstract class AntenaView extends Group
{
    /**
     * Constructor de una antenaview. Puede ser construida como solamente un dibujo, o como una antena completa
     * funcional, con un modelo ligado.
     * @param home          Instancia de Sim a la cual agregar la antena
     * @param movable       Booleano, true si la view es una antena concreta, false si es sólo una imagen.
     */
    public AntenaView(Sim home, Boolean movable)
    {
        if (!movable) return;
        contextMenu = new ContextMenu();
        links = new ArrayList<LinkLine>();

        EventHandler<MouseEvent> openContextHandler = event -> {
            if (event.getButton() == MouseButton.SECONDARY){
                updateSnapTo();
                contextMenu.show(this,event.getScreenX(),event.getScreenY());
            }
            else contextMenu.hide();
            event.consume();
        };
        this.setOnMouseClicked(openContextHandler);


        MenuItem editAntena = new MenuItem("Edit antena");
        editAntena.setOnAction(event->new Antena_Stage(home,model));

        MenuItem move = new MenuItem("Move");
        move.setOnAction(event->{
            clearLinks();
            //Este circulo ayuda a que la antena no salga del mouse al moverse
            Circle moveRange = new Circle(0,0,width*2);
            moveRange.setFill(Color.TRANSPARENT);
            //spinRange.setStroke(Color.RED);
            getChildren().add(moveRange);
            //Ligar antena a posición del mouse
            setOnMouseMoved(mouse->{
                setLayoutX(mouse.getSceneX());
                setLayoutY(mouse.getSceneY());
            });
            //Dejar la antena y actualizar modelo
            setOnMouseClicked(mouse->{
                getChildren().remove(moveRange);
                model.move(getLayoutX(),getLayoutY());
                System.out.println("Moved antena to: "+model.getX()+","+model.getY());
                setOnMouseMoved(null);
                //Volver a funcionamiento normal del menu de contexto
                setOnMouseClicked(openContextHandler);
            });

        });
        //Similar a move pero para rotar.
        MenuItem rotate = new MenuItem("Rotate");
        rotate.setOnAction(event->{
            clearLinks();
            //Este circulo es invisible y solo expande el rango de deteccion del mouseEventHandler.
            Circle spinRange = new Circle(0,0,1000);
            spinRange.setFill(Color.TRANSPARENT);
            spinRange.setStroke(Color.RED);
            getChildren().add(spinRange);
            setOnMouseMoved(mouse-> {
                //Se debe restar el centro al absoluto en vez de usar coordenadas relativas ya que estas
                //van cambiando al rotar.
                setRotate(new Angle((mouse.getSceneX()-centerX),(mouse.getSceneY()-centerY)).getDeg());
            });
            setOnMouseClicked(mouse -> {
                model.rotate(new Angle((mouse.getSceneX()-centerX),(mouse.getSceneY()-centerY)));
                System.out.println("Rotated antena to: "+model.getAngle().getDeg());
                getChildren().remove(spinRange);
                setOnMouseMoved(null);
                setOnMouseClicked(openContextHandler);
            });
        });

        //Entrega una ventana para ingresar (x,y) y mueve a esa coordenada.
        MenuItem moveTo = new MenuItem("Move to");
        moveTo.setOnAction(e->{Move_Stage m = new Move_Stage(this.model);});
        //Permite elejir una antena a la cual apuntar directamente.
        snapTo = new Menu("Snap to",null);
        snapTo.getStyleClass().add("sub-menu");

        contextMenu.getItems().addAll(move,rotate,editAntena,moveTo,snapTo);
    }


    /**
     * Fija el modelo de la antenaview. Se actualiza tambien la opción de reproducción de audio y la
     * musetra en pantalla de potencia a traves de labels.
     * @param model         Modelo de la antena
     */
    public void setModel(Antena model){
        this.model = model;
        nameLabel = new Label(model.getName());
        double toShow;
        if (Sim.useDBM){toShow=10*Math.log10(model.getPotencia()*1000);} else toShow = model.getPotencia();
        if (!model.isTransmiting()){
            MenuItem play = new MenuItem("Play Audio");
            play.setOnAction(e->{
                Media audio = Sim.getSound();
                MediaPlayer player = new MediaPlayer(audio);
                //r mapea atenuacion tal que 0.1 es 1/2, 0.01 es 1/3, 0.01 es 1/4, etc...
                double r;
                try {r = 1/ (1+Math.log10(model.getReference()/model.getPotencia()));}
                catch (Exception exception){r = 0;}
                System.out.println("Playing at "+100*r+"% volume");
                player.setVolume(r);
                player.play();
            });
            contextMenu.getItems().addAll(play);
            powLabel = new Label("Recieving: ");
        }
        else {
            powLabel = new Label("Transmiting: "+toShow);
        }
    }

    /**
     * Fija la instancia de Sim de la antenaview. Esta se usa para poder mostrar correctamente
     * los label de potencia, sin que este rote con la antena misma.
     * @param home          Instancia de Sim en uso
     */
    public void setHome(Sim home){
        this.home = home;
        labelBox = new VBox();
        labelBox.getChildren().addAll(nameLabel,powLabel);
        home.getChildren().add(labelBox);
        nameLabel.setFont(new Font(20));
        powLabel.setFont(new Font(20));
        powLabel.setMinWidth(Region.USE_PREF_SIZE);
        powLabel.setMaxWidth(Region.USE_PREF_SIZE);
        nameLabel.setMinWidth(Region.USE_PREF_SIZE);
        nameLabel.setMaxWidth(Region.USE_PREF_SIZE);
        labelBox.relocate(centerX+80,centerY+80);
    }

    /**
     * Maneja la parte visual de los enlaces entre dos antenas, además de actualizar la potencia recibida. Este
     * metodo solo debería llamarse en el receptor.
     * @param Tx            Antena transmisora ligada
     * @param errorFlag     Booleano para indicar si el resultado es matematicamente inválido
     */
    public void link(AntenaView Tx, boolean errorFlag){
        //Aqui se maneja toda la parte visual de los enlaces entre las antenas
        LinkLine link = new LinkLine(this,Tx);
        home.getChildren().add(0,link); //El 0 asegura que se agreguen al inicio del arreglo y por tanto no tapen las antenas
        links.add(link);
        double toShow;
        if (Sim.useDBM){toShow=10*Math.log10(model.getPotencia()*1000);} else toShow = model.getPotencia();
        powLabel.setText("Recieving: " + toShow);
        if (errorFlag){
            powLabel.setText("INVALID RESULT");
        }
    }

    /**
     * Elimina los enlaces visuales entre las antenas y detiene sus timers. Esto se debe hacer siempre
     * que se eliminen o modifiquen enlaces, pues los enlaces usan timers que pueden quedar corriendo.
     */
    public void clearLinks(){
        for (int i = 0; i < links.size(); i++)
        {
            home.getChildren().remove(links.get(i));
            links.get(i).stop();
            links.remove(links.get(i));
        }
    }

    /**
     * Actualiza la potencia mostrada en un transmisor, ya que ahora esta puede cambiar por si se usa DBM o Watt.
     */
    public void txupdate()
    {
        if (!model.isTransmiting()) return;
        if (Sim.useDBM){powLabel.setText("Transmiting: "+10*Math.log10(model.getPotencia()*1000));}
        else {powLabel.setText("Transmiting: "+model.getPotencia());}
    }

    /**
     * Retorna la coordenada X del centro de la antena, el cual es el usado en los calculos de transferencia.
     * @return centerX      coordenada X del centro de la antena
     */
    public double getCenterX() {return centerX;}

    /**
     * Define la coordenada X del centro de la antena, el cual es el usado en los calculos de transferencia.
     * @param centerX       nueva coordenada X del centro de la antena
     */
    public void setCenterX(double centerX) {this.centerX = centerX;}

    /**
     * Retorna la coordenada Y del centro de la antena, el cual es el usado en los calculos de transferencia.
     * @return centerY      coordenada X del centro de la antena
     */
    public double getCenterY() {return centerY;}

    /**
     * Define la coordenada Y del centro de la antena, el cual es el usado en los calculos de transferencia.
     * @param centerY       nueva coordenada Y del centro de la antena
     */
    public void setCenterY(double centerY) {this.centerY = centerY;}

    /**
     * Retorna el ancho de la imágen de la antena dibujada visualmente.
     * @return  width       ancho de el dibujo de la antena
     */
    public int getWidth() {return width;}

    /**
     * Retorna el alto de la imágen de la antena dibujada visualmente.
     * @return height       alto de el dibujo de la antena
     */
    public int getHeight() {return height;}

    /**
     * Retorna el Pane donde se muestran los label de nombre y potencia de transferencia de la antena.
     * @return labelbox     Vbox donde se encuentran los label de la antena
     */
    public VBox getLabelBox(){return labelBox;}

    /**
     * Retorna el modelo de antena de la vista en cuestión.
     * @return model        modelo de la vista
     */
    public Antena getModel(){return model;}

    private void updateSnapTo(){
        snapTo.getItems().clear();
        for (Antena antenaX: Sim.getAntenas())
        {
            if (antenaX.getView()!=this){
                MenuItem n = new MenuItem(antenaX.getName());
                n.setOnAction(e->{
                    model.rotate(new Angle((antenaX.getX()-centerX),(antenaX.getY()-centerY)));
                    System.out.println(new Angle((antenaX.getX()-centerX),(antenaX.getY()-centerY)).getDeg());
                });
                snapTo.getItems().add(n);
            }
        }
    }

    private Menu snapTo;
    private Sim home;
    private Label nameLabel;
    private Label powLabel;
    private VBox labelBox;
    private ContextMenu contextMenu;
    private ArrayList<LinkLine> links;
    /**
     * Ancho del dibujo que reprenseta la vista. Sólo toma
     * un valor en los constructores de clase hijo.
     */
    protected int width;
    /**
     * Alto del dibujo que reprenseta la vista. Sólo toma
     * un valor en los constructores de clase hijo.
     */
    protected int height;
    private Antena model;
    private double centerX;
    private double centerY;


}
