import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.stage.Stage;

///////   Config_Stage.java    ///////

/**
 * Representa la ventana que se abre para editar los parametros de la simulación.
 * Extiende la clase Stage de javaFx y su único uso en el código es llamar a su constructor. El resto de los metodos
 * son privados e internos.
 */

public class Config_Stage extends Stage
{
    /**
     * Constructor de la Config_Stage. No recibe parametro, pero cuando se elije el botón confirmar
     * se editan los parametros de la simulación y se actualizan las transferencias.
     */
    public Config_Stage(){
        setAlwaysOnTop(true);
        setHeight(250);
        setWidth(400);
        setResizable(false);

        VBox conf = new VBox();
        conf.getChildren().addAll(makeFrecIn(),
                                    makeAudIn(),
                                    makeWeathIn(),
                                    makeScaleIn(),
                                    makeDBMin());
        conf.setPadding(new Insets(20,20,20,20));


        HBox buttons = new HBox();
        Button exit = new Button("Exit");
        exit.setOnAction(e->this.close());

        Button confirm = new Button("Confirm");
        //Enter shortcut
        addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode()== KeyCode.ENTER){confirm.fire();}
            else if (event.getCode()==KeyCode.ESCAPE){exit.fire();}
        });
        confirm.setOnAction(e->{
            Sim.frec = Double.parseDouble(frecIn.getText());
            Sim.sound = new Media(audIn.getText());
            Sim.weathAttenuation = 1; //TODO!
            Sim.scale = new Scale((String) scaleIn.getValue());
            Sim.useDBM = dCheck.isSelected();
            this.close();
            Sim.updateTransference();
        });
        buttons.getChildren().addAll(confirm,exit);

        BorderPane main = new BorderPane();
        main.setTop(conf);
        main.setBottom(buttons);
        Scene scene = new Scene(main);
        setTitle("Configuration");
        setScene(scene);
        show();
    }
    private HBox makeFrecIn(){
        HBox frecLine = new HBox();
        frecIn = new TextField();
        frecIn.setText(String.valueOf(Sim.getFrec()));
        frecLine.getChildren().addAll(new Label("Frequency:  "),frecIn,new Label(" [hz]"));
        return frecLine;
    }
    private HBox makeAudIn(){
        HBox audLine = new HBox();
        audIn = new TextField();
        audIn.setPrefWidth(300);
        audIn.setText(Sim.getSound().getSource().toString());
        audLine.getChildren().addAll(new Label("Audio file:  "),audIn);
        return audLine;
    }
    private HBox makeWeathIn(){
        HBox weathLine = new HBox();
        String optsW[] = {"Sunny","Cloudy","Raining","Snowing"};
        weathIn = new ComboBox(FXCollections.observableArrayList(optsW));
        weathIn.setValue("Sunny");
        weathLine.getChildren().addAll(new Label("Weather:  "),weathIn);
        return weathLine;
    }
    private HBox makeScaleIn(){
        HBox scaleLine = new HBox();
        scaleIn = new ComboBox(FXCollections.observableArrayList(Scale.getopts()));
        scaleIn.setValue(Sim.getScale().getString());
        scaleLine.getChildren().addAll(new Label("Scale:  1000 pixels are "),scaleIn);
        return scaleLine;
    }
    private HBox makeDBMin(){
        HBox dbLine = new HBox();
        dCheck = new CheckBox();
        dCheck.setSelected(Sim.useDBM);
        dbLine.getChildren().addAll(new Label("Use DBM ",dCheck));
        return dbLine;
    }

    private CheckBox dCheck;
    private TextField frecIn;
    private TextField audIn;
    private ComboBox weathIn;
    private ComboBox scaleIn;

}
