import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

///////   Proyecto.java    ///////

/**
 * Clase principal del proyecto. Extiendo la clase Application de java
 * y contiene un mínimo de configuración del programa.
 */

public class Proyecto extends Application
{
    /**
     * Método donde se inicia la ejecución del programa. Se define
     * las caracteristicas del primaryStage y se instancia la simulación.
     * @param primaryStage      Stage principal. Es otorgado por JavaFx
     */
    public void start(Stage primaryStage)
    {
        Sim mainS= new Sim();
        Scene scene = new Scene(mainS);
        primaryStage.setTitle("Simulador de antenas");
        primaryStage.getIcons().add(new Image("resources/icon.png"));
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(e->{
            System.out.println("Closing");
            for (Antena antena : mainS.getAntenas())
            {
                antena.getView().clearLinks();
            }
            System.exit(0);
        });

        Config_Stage c = new Config_Stage();
        primaryStage.show();
    }

}