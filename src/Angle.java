///////   Angle.java    ///////

/**
 * Tipo de dato no primitivo que describe el ángulo de inclinación con respecto al horizonte en dirección horaria.
 * Permite retornar su magnitud en grados o radianes y permite obtener ángulo estáticos de Pi y Pi/2.
 */

public class Angle
{
    /**
     * Constructor Angle por tamaño de ángulo
     * @param val             Valor del ángulo (se guarda internamente en grados)
     * @param isInDeg         Indica si el valor entrado está en grados (alternativamente puede estar en radianes)
     */
    public Angle(double val, boolean isInDeg)
    {
        setVal(val,isInDeg);
    }

    /**
     * Constructor Angle por coordenadas de catetos
     * @param x         Largo del cateto adyacente al ángulo
     * @param y         Largo del cateto opuesto al ángulo
     */
    public Angle(double x, double y){
        if (x>0){
            setVal(Math.atan(y/x),false);
            return;
        }
        if (x<0 && y>0){
            setVal(Math.atan(y/x)-Math.PI*(3/2),false);
            return;
        }

        if (x<0 && y<0){
            setVal(Math.PI+Math.atan(y/x),false);
            return;
        }
    }
    /**
     * Definir el valor del ángulo. Lo normalisa si es necesario de radianes a grados como un ángulo entre 0 y 360
     * @param val             Valor del ángulo (se guarda internamente en grados)
     * @param isInDeg         Indica si el valor entrado está en grados (alternativamente puede estar en radianes)
     */
    public void setVal(double val ,boolean isInDeg)
    {
        if (!isInDeg)
        {
            val = val*180/Math.PI;
        }
        while (val < 0)
        {
            val = 360 + val;
        }
        while (val >360)
        {
            val = val - 360;
        }
        this.val = val;
    }
    /**
     * Recibir el valor del ángulo en grados [0,360]
     * @return  val             Valor del ángulo (se guarda internamente en grados)
     */
    public double getDeg(){return val;}
    /**
     * Recibir el valor del ángulo en radianes [0,2pi]
     * @return  val             Valor del ángulo (se guarda internamente en grados)
     */
    public double getRad(){return val*Math.PI/180;}

    /**
     * Define si 2 ángulos son iguales
     * @return  isEqual         Valor booleano de la igualdad
     */
    @Override
    public boolean equals(Object obj)
    {
        if (obj instanceof Angle) return this.val == ((Angle) obj).val;
        else return false;
    }

    /**
     * Restar el ángulo en argumento al ángulo actual
     * @param   rest            Ángulo a restar
     * @return  result          Nuevo ángulo resultante (normalizado a [0,360]
     */
    public Angle minus(Angle rest){
        return new Angle(getDeg() - rest.getDeg(),true);
    }
    /**
     * Sumar el ángulo en argumento al ángulo actual
     * @param   sum             Ángulo a sumar
     * @return  result          Nuevo ángulo resultante (normalizado a [0,360]
     */
    public Angle plus(Angle sum){
        return new Angle(getDeg() + sum.getDeg(),true);
    }
    /**
     * Comparar los si el angulo es menor que un 2do ángulo
     * @param   mayor           Ángulo de referencia
     * @return  isMenor         True si el ángulo mayor es mayor, false si es menor
     */
    public boolean lessThan(Angle mayor){
        return getDeg() < mayor.getDeg();
    }
    /**
     * Comparar los si el angulo es mayor que un 2do ángulo
     * @param   mayor           Ángulo de referencia
     * @return  isMenor         True si el ángulo mayor es menor, false si es mayor
     */
    public boolean moreThan(Angle mayor){
        return getDeg() > mayor.getDeg();
    }
    /**
     * Ángulo complementario al ángulo de referencia
     * @return  complemento      Ángulo que al sumar el ángulo de referencia resulta 360
     */
    public Angle restofRotation(){
        return new Angle(360-val,true);
    }
    /**
     * Ángulo equivalente a Pi radianes, 180 grados o 0.5 revoluciones
     * @return  Pi              Ángulo de magnitud Pi radianes
     */
    public static Angle Pi(){
        return new Angle(Math.PI,false);
    }
    /**
     * Ángulo equivalente a Pi/2 radianes, 90 grados o 0.25 revoluciones
     * @return  Pi/2              Ángulo de magnitud Pi/2 radianes
     */
    public static Angle PiHalf(){
        return new Angle(Math.PI/2,false);
    }

    private double val;
}
