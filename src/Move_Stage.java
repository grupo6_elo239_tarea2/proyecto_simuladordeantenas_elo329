import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

///////   Antena_Stage.java    ///////

/**
 * Representa la ventana que se abre para mover una antena a una posición especifica. Extiende la clase Stage
 * de javaFx y su único uso en el código es llamar a su constructor.
 */

public class Move_Stage extends Stage
{
    /**
     * Constructor de un nuevo movestage. Pide solamente la antena que se va
     * a manipular, y permite moverla usando los botones mostrados.
     * @param A         Antena a mover
     */
    public Move_Stage(Antena A){
        setAlwaysOnTop(true);
        setWidth(300);
        setHeight(100);

        VBox window = new VBox();

        HBox xbox = new HBox();
        Spinner<Double> xIn = new Spinner<Double>();
        xIn.setValueFactory(
                new SpinnerValueFactory.DoubleSpinnerValueFactory(0,4000,0));
        xIn.setEditable(true);
        xbox.getChildren().addAll(new Label("X: "),xIn);
        HBox ybox = new HBox();
        Spinner<Double> yIn = new Spinner<Double>();
        yIn.setValueFactory(
                new SpinnerValueFactory.DoubleSpinnerValueFactory(0,4000,0));
        yIn.setEditable(true);
        ybox.getChildren().addAll(new Label("Y: "),yIn);

        HBox buttons = new HBox();
        Button finButton = new Button("Mover");
        Button closeButton = new Button("Cancel");
        buttons.getChildren().addAll(finButton,closeButton);
        closeButton.setOnAction(e->this.close());
        finButton.setOnAction(e->{
            A.move(xIn.getValue(),yIn.getValue());
        });

        addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode()== KeyCode.ENTER){finButton.fire();}
            else if (event.getCode()==KeyCode.ESCAPE){closeButton.fire();}
        });

        window.getChildren().addAll(xbox,ybox,buttons);
        setTitle("Move: "+A.getName());
        Scene scene = new Scene(window);
        setScene(scene);
        show();
    }
}
