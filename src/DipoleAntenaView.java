import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

///////   DipoleAntenaView.java    ///////

/**
 * Vista de una antena de tipo dipolo. En su implementación solo realiza el dibujo de el dipolo y
 * llama el constructor de AntenaView. Extiende la clase AntenaView y puede actuar como vista para
 * una DipoleLambdaAntena o una DipoleHalfLambdaAntena.
 */

public class DipoleAntenaView extends AntenaView
{
    /**
     * Constructor de la vista de una antena dipolo. Llama al constructor de AntenaView y luego
     * construye el dibujo de forma apropiada.
     * @param home          Instancia de Sim a la cual agregar la vista
     * @param movable       Booleano, true si es movible y funciona, false si es sólo un dibujo
     */
    public DipoleAntenaView(Sim home, Boolean movable)
    {
        super(home, movable);
        width = 100;
        height = width;

        Rectangle coax = new Rectangle(width*0.95,height*1/5);
        coax.relocate(-width/2, -coax.getHeight()/2);
        coax.setFill(Color.DARKGRAY);
        coax.setStroke(Color.GREY);
        Rectangle left = new Rectangle(width/10,height*2/5);
        left.relocate((width/2)-width/10,-height/2);
        left.setFill(Color.DARKGRAY);
        left.setStroke(Color.GREY);
        Rectangle right = new Rectangle(width/10,height*2/5);
        right.relocate((width/2)-width/10,(width*3/5)-height/2 );
        right.setFill(Color.DARKGRAY);
        right.setStroke(Color.GREY);

        getChildren().addAll(left,right,coax);
    }
}